/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class SuperTester {
    public static class Level1{
        public int m=45;
        public int get(){
            return 1;
        }
    }
    public static class Level2 extends  Level1{
        public int m=46;
        @Override
        public int get(){
            return 2;
        }
        public int superGet(){
            return super.get();
        }
        public int getSuperM(){
            return super.m;
        }
    }
    public static void test(){
        Level2 l2=new Level2();
        assert(l2.get()==2):"Error in super class";
        assert(l2.superGet()==1):"Error in super class";
        assert(l2.m==46):"Error in super access";
        assert(l2.getSuperM()==45):"Error in super access";
    }

}
