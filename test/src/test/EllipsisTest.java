/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author Deepak Patil
 */
public class EllipsisTest {
    
    public static void test(){
        assert(add()==0):"Error in ellipse parameters";
        assert(add(5,5)==20):"Error in ellipse parameters";
        assert(add(4,5,6)==15):"Error in ellipse parameters";        
    }
   
    public static int add(int... numbers){
        int rvalue=0;
        for(int i:numbers){
            rvalue += i;
        }
        return rvalue;
    }
    public static int add(int a,int b){        
        return a+b+10;
    }
    public static int add2(int k,int... numbers){
        int rvalue=0;
        for(int i:numbers){
            rvalue += i;
        }
        return rvalue;
    }        
}
