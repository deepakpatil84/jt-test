/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class InterfaceOverrideTest {
    static interface One{
        public void get();
    }
    static interface Two extends One{
        public void get();
    }
    static class TwoImpl implements Two{
        public void get(){
            
        }
    }
    public void Test(){

    }

}
