/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class EnumTest {
    public interface EnumInterface<K>{
        public K hello();
    }
    public enum MyEnum2{
        ONE,TWO1;
    }

    public enum MyEnum implements EnumInterface<Integer>{
        //DEEPAK(2){public int get(int n){return this.hello();};},
        //PATIL(3){public int get(int n){return n;};};
        ONE(1),
        TWO(2),
        THREE(3);

        private int m;
        MyEnum(int k){
            this.m=k;
        }
        public Integer hello(){
            return m;
        }
        public class Deepak{
            
        }
        public static MyEnum getValue(){
            return ONE;
        }
    }
    
    public static void hello(){
        MyEnum2 m=MyEnum2.ONE;
        int k=0;
        final int l=0;
        switch(k){
            case l:k=2;
        }
        switch(m){
            case ONE: k=1; break;
            case TWO1: k=2; break;
          
        }
        //assert(m.name().equals("ONE")):"Error in enum name";
    }

}
