/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class InnerCreator {

    int outerValue;
    InnerCreator ic;
    
    public InnerCreator(int va){
        outerValue=va;
    }
    public interface InnerInterface{
        public int getParentValue();
    }
    public class Inner{
        public int innerValue;
        public Inner(int va){
            innerValue=va;
        }
        public int getInnerValue(){
            return innerValue;
        }
        public int getOuterValue(){
            return outerValue;
        }
    }
    public Inner getInnerInstance(){
        return ic.new Inner(34);
    }
    // just to test a compiler , weired case
    public Inner getInnerInstance2(){
       return ic.new Inner(34);
    }
    public Inner getInnerInstanceWithSelf(){
       return new Inner(34);
    }
    public InnerCreator createIC(){
        ic=new InnerCreator(800);
        return ic;
    }
}
