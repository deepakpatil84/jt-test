/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import test.ExCreator.Access;

/**
 *
 * @author Deepak Patil
 */
public class Test {

    String zzzz = "welcome";
    String xxx = zzzz;
   

    public static String testString() {
        String s = new String();

        assert (s.length() == 0) : "Error in creating empty String";
        byte[] bytes = new byte[]{'a', 'b', 'c'};
        s = new String(bytes);
        assert (s.length() == 3) : "Error in creating String from bytes";
        assert (s.equals("abc")) : "Error in creating String from bytes";
        s = new String(bytes, 1, 1);
        assert (s.length() == 1) : "Error in creating String from bytes";
        assert (s.equals("b")) : "Error in creating String from bytes";
        char[] chars = new char[]{'a', 'b', 'c'};
        s = new String(chars);
        assert (s.length() == 3) : "Error in creating String from chars";
        assert (s.equals("abc")) : "Error in creating String from chars";
        s = new String(chars, 1, 1);
        assert (s.length() == 1) : "Error in creating String from chars";
        assert (s.equals("b")) : "Error in creating String from chars";
        String ns = new String(s);
        assert (ns.equals(s)) : "Error in creating Copy of string";
        ns = new String("welcome");
        assert (ns.equals("welcome")) : "Error in creating Copy of string";
        //charAt
        assert (ns.charAt(0) == 'w') : "Error in charAt";
        assert (ns.codePointAt(0) == 119) : "Error in codePointAt";
        assert (ns.codePointBefore(1) == 119) : "Error in codePointBefore";

        assert (ns.codePointCount(0, 7) == 7) : "Error in codePointCount";
        chars = new char[]{'\u0950'};
        s = new String(chars);
        assert (s.codePointCount(0, 1) == 1) : "Error in codePointCount";

        //compareTo
        assert (ns.compareTo("welcome") == 0) : "Error in compareTo";
        assert (ns.compareTo("welcome1") == -1) : "Error in compareTo";
        assert (ns.compareTo("welcom") == 1) : "Error in compareTo";

        //compareToIgnoreCase
        assert (ns.compareToIgnoreCase("WeLCome") == 0) : "Error in compareToIgnoreCase";
        assert (ns.compareToIgnoreCase("wElCome1") == -1) : "Error in compareToIgnoreCase";
        assert (ns.compareToIgnoreCase("WelcoM") == 1) : "Error in compareToIgnoreCase";

        //concat
        assert ("wel".concat("come").equals("welcome")) : "Error in concat";
        assert (ns.concat("123").equals("welcome123")) : "Error in concat";

        //contains
        assert (ns.contains("lco")) : "Error in contains";
        assert (ns.contains("456") == false) : "Error in contains";

        //contentEquals
        ns = new String("welcome");
        assert (ns.contentEquals("welcome")) : "error in contentEquals";
        assert (ns.contentEquals("welcome123") == false) : "error in contentEquals";

        chars = new char[]{'a', 'b', 'c'};
        //copyValueOf
        assert (String.copyValueOf(chars).equals("abc")) : "Error in copyValueOf";
        assert (String.copyValueOf(chars).equals("123") == false) : "Error in copyValueOf";
        assert (String.copyValueOf(chars, 1, 1).equals("b")) : "Error in copyValueOf";
        assert (String.copyValueOf(chars, 1, 1).equals("a") == false) : "Error in copyValueOf";

        //endsWith
        assert (ns.endsWith("me")) : "Error in endsWith";
        assert (ns.endsWith("123") == false) : "Error in endsWith";
        assert (ns.endsWith("") == true) : "Error in endsWith";
        assert (ns.endsWith(null) == false) : "Error in endsWith";

        //equals
        assert (ns.equals("welcome")) : "Error in equals";
        assert (ns.equals("welcome123") == false) : "Error in equals";
        assert (ns.equals(null) == false) : "Error in equals";

        //equalsIgnoreCase
        assert (ns.equalsIgnoreCase("WeLCome")) : "Error in equalsIgnoreCase";
        assert (ns.equalsIgnoreCase("wElCome1") == false) : "Error in equalsIgnoreCase";
        assert (ns.equalsIgnoreCase(null) == false) : "Error in equalsIgnoreCase";
        bytes = new byte[]{123, 123};
        ns = new String(bytes);

        //getBytes
        assert (ns.getBytes()[0] == 123) : "Error in getBytes";
        char[] dst = new char[1];
        chars = new char[]{'a', 'b'};
        ns = new String(chars);

        //getChars
        ns.getChars(0, 1, dst, 0);
        assert (dst[0] == 'a') : "Error in getChars";

        //indexOf
        ns = new String("welcome");
        assert (ns.indexOf('w') == 0) : "Error in indexOf";
        assert (ns.indexOf('e') == 1) : "Error in indexOf";
        assert (ns.indexOf('x') == -1) : "Error in indexOf";

        assert (ns.indexOf('l', 1) == 2) : "Error in indexOf";
        assert (ns.indexOf('w', 1) == -1) : "Error in indexOf";
        assert (ns.indexOf('x') == -1) : "Error in indexOf";

        assert (ns.indexOf("wel") == 0) : "Error in indexOf";
        assert (ns.indexOf("come") == 3) : "Error in indexOf";
        assert (ns.indexOf("x") == -1) : "Error in indexOf";

        assert (ns.indexOf("lc", 0) == 2) : "Error in indexOf";
        assert (ns.indexOf("wel", 2) == -1) : "Error in indexOf";
        assert (ns.indexOf("x", 0) == -1) : "Error in indexOf";

        s = ns.intern();
        //intern
        assert (s.equals(ns)) : "Error in intern";

        //isEmpty
        assert (s.isEmpty() == false) : "Error in isEmpty";
        assert ("".isEmpty()) : "Error in isEmpty";

        //lastIndexOf
        assert (ns.lastIndexOf('e') == 6) : "Error in lastIndexOf";
        assert (ns.lastIndexOf('e', 4) == 1) : "Error in lastIndexOf";
        assert (ns.lastIndexOf("e") == 6) : "Error in lastIndexOf";
        assert (ns.lastIndexOf("e", 4) == 1) : "Error in lastIndexOf";

        //length
        assert (ns.length() == 7) : "Error in length";
        //TODO:matches

        //TODO:offsetByCodePoints dont know what this function do
        //int a=ns.offsetByCodePoints(4, 'e');
        //TODO:regionMatches

        //replace
        s = ns.replace("e", "XX");
        assert (s.equals("wXXlcomXX")) : "Error in replace";

        //replaceAll
        s = ns.replaceAll("e", "XX");
        assert (s.equals("wXXlcomXX")) : "Error in replace";

        //replaceFirst
        s = ns.replaceFirst("e", "XX");
        assert (s.equals("wXXlcome")) : "Error in replace";

        //split
        s = new String("a|b|c||");
        String[] ss = s.split("\\|");
        assert (ss.length == 3) : "Error in string split";
        ss = s.split("x");
        assert (ss.length == 1) : "Error in string split";
        assert (ss[0].equals("a|b|c||")) : "Error in string split";

        s = "welcome";
        //startsWith
        assert (s.startsWith("wel")) : "Error in startsWith";
        assert (s.startsWith("com") == false) : "Error in statswith";
        assert (s.startsWith("come", 3)) : "Error in startsWith";
        assert (s.startsWith("come", 2) == false) : "Error in statswith";

        //substring
        assert (s.substring(0).equals("welcome")) : "Error in substring";
        assert (s.substring(1).equals("elcome")) : "Error in substring";
        assert (s.substring(0, 2).equals("we")) : "Error in substring";
        assert (s.substring(1, 4).equals("elc")) : "Error in substring";

        //toCharArray
        chars = s.toCharArray();
        assert (chars.length == 7) : "Error in toCharArray";
        assert (chars[0] == 'w') : "Error in toCharArray";
        assert (chars[1] == 'e') : "Error in toCharArray";
        assert (chars[2] == 'l') : "Error in toCharArray";
        assert (chars[3] == 'c') : "Error in toCharArray";
        assert (chars[4] == 'o') : "Error in toCharArray";
        assert (chars[5] == 'm') : "Error in toCharArray";
        assert (chars[6] == 'e') : "Error in toCharArray";

        s = "weLcomE";
        //toLowerCase
        assert (s.toLowerCase().equals("welcome")) : "Error in toLowerCase";

        //TODO:toLowerCase(local)

        //toString
        assert (s.toString().equals("weLcomE")) : "Error in toString";
        assert (s.toUpperCase().equals("WELCOME")) : "Error in toUpperCase";

        //trim
        s = " welcome ";
        assert (s.trim().equals("welcome")) : "Error in trim";

        //valueOf(boolean)
        assert (String.valueOf(true).equals("true")) : "Error in valueOf(boolean)";
        assert (String.valueOf(false).equals("false")) : "Error in valueOf(boolean)";
        //valueOf(char)
        assert (String.valueOf('a').equals("a")) : "Error in valueOf(char)";
        chars = new char[]{'a', 'b', 'c'};
        //valueOf(chars[])
        assert (String.valueOf(chars).equals("abc")) : "Error in valueOf(chars[])";
        //valueOf(chars[],offset,count
        assert (String.valueOf(chars, 1, 1).equals("b")) : "Error in valueOf(chars[],offset,count)";
        double d = 8;
        assert (String.valueOf(d).equals("8")) : "Error in valueOf(double)";
        float f = 5 / 2;
        assert (String.valueOf(f).equals("2.5")) : "Error in valueOf(float)";
        int i = 5;
        assert (String.valueOf(i).equals("5")) : "Error in valueOf(int)";
        long l = 5;
        assert (String.valueOf(l).equals("5")) : "Error in valueOf(long)";
        assert ("DEEPAK".hashCode() == 2012639766) : "Error in hash code";
        assert ("PATIL".hashCode() == 75901158) : "Error in hash code";
        assert ("".hashCode() == 0) : "Error in hash code";
        assert ("jstype".hashCode() == -1263171502) : "Error in hash code";
        assert ("OPENWAF".hashCode() == -545188814) : "Error in hash code";
        return "OK";

    }

    public static String testCharacter() {
        Character ch = new Character('b');
        char[] chars = new char[]{'a', 'b', 'c'};
        assert (Character.charCount('a') == 1) : "Error in charCount";
        assert (ch.charValue() == 'b') : "Error in charValue";
        assert (Character.codePointAt(chars, 0) == 'a') : "Error in codePointAt";
        assert (Character.codePointAt(chars, 0, 1) == 'a') : "Error in codePointAt";
        assert (Character.codePointBefore(chars, 2) == 'b') : "Error in codePointBefore";
        assert (Character.codePointBefore(chars, 2, 1) == 'b') : "Error in codePoinBefore";
        assert (Character.codePointBefore("abc", 2) == 'b') : "Error in codePoinBefore";
        assert (Character.codePointCount(chars, 0, 3) == 3) : "Error in codePointCount";
        assert (Character.codePointCount("abc", 0, 3) == 3) : "Error in codePointCount";
        assert (ch.compareTo(new Character('b')) == 0) : "Error in compareTo";
        assert (ch.compareTo(new Character('a')) == 1) : "Error in compareTo";
        assert (ch.compareTo(new Character('c')) == -1) : "Error in compareTo";
        assert (Character.digit('1', 10) == 1) : "Errror in digit";
        assert (Character.digit('a', 16) == 10) : "Errror in digit";
        assert (Character.digit('A', 16) == 10) : "Errror in digit";
        assert (Character.digit(49, 10) == 1) : "Errror in digit";
        assert (Character.digit(98, 16) == 11) : "Errror in digit";
        assert (Character.digit(65, 16) == 10) : "Errror in digit";
        assert (ch.equals(new Character('b'))) : "Error in equals";
        assert (ch.equals(new Character('c')) == false) : "Error in equals";
        assert (Character.forDigit(0, 10) == '0') : "Error in forDigit";
        assert (Character.forDigit(11, 16) == 'b') : "Error in forDigit";
        assert (Character.forDigit(16, 16) == 0) : "Error in forDigit";
        assert (Character.getNumericValue('a') == 97) : "Error in getNumericValue";
        assert (Character.getNumericValue(97) == 97) : "Error in getNumericValue";
        assert (Character.isDigit('5') == true) : "Error in isDigit";
        assert (Character.isDigit('A') == false) : "Error in isDigit";
        assert (Character.isDigit(49) == true) : "Error in isDigit";
        assert (Character.isDigit(60) == false) : "Error in isDigit";
        assert (Character.isHighSurrogate('a') == false) : "Error in isHighSurrogate";
        assert (Character.isHighSurrogate('\uD801') == true) : "Error in isHighSurrogate";
        assert (Character.isLetter('a') == true) : "Error in isLetter";
        assert (Character.isLetter('1') == false) : "Error in isLetter";
        assert (Character.isLetterOrDigit('a') == true) : "Error in isLetterOrDigit";
        assert (Character.isLetterOrDigit('1') == true) : "Error in isLetterOrDigit";
        assert (Character.isLetterOrDigit('>') == false) : "Error in isLetterOrDigit";
        assert (Character.isLowerCase('a')) : "Error in isLowerCase";
        assert (!Character.isLowerCase('A')) : "Error in isLowerCase";
        assert (!Character.isLowSurrogate('\uD801')) : "Error in isLowSurrogate";
        assert (Character.isSpace(' ')) : "Error in isSpace";
        assert (!Character.isSpace('a')) : "Error in isSpace";
        assert (!Character.isSupplementaryCodePoint('a')) : "Error in isSupplementaryCodePoint";
        //TODO:isSurrogatePair
        assert (!Character.isUpperCase('a')) : "Error in isUpperCase";
        assert (Character.isUpperCase('A')) : "Error in isUpperCase";
        assert (Character.isValidCodePoint(97)) : "Error in isValidCodePoint";
        assert (!Character.isValidCodePoint(-1)) : "Error in isValidCodePoint";
        assert (Character.isWhitespace(' ')) : "Error in isWhitespace";
        assert (!Character.isWhitespace('x')) : "Error in isWhitespace";
        assert (Character.isWhitespace((char) 9)) : "Error in isWhitespace";
        assert (!Character.isWhitespace((char) 65)) : "Error in isWhitespace";
        //TODO:offsetByCodePoints
        assert (Character.toChars('a')[0] == 'a') : "Error in toChars";
        assert (Character.toChars(97)[0] == 'a') : "Error in toChars";
        char[] dst = new char[4];
        Character.toChars(97, dst, 2);
        assert (dst[2] == 97) : "Error in to toChars";
        assert (Character.toString('a').equals("a")) : "Error in toString";
        assert (Character.valueOf('b').compareTo(ch) == 0) : "Error in valueOf";


        return "OK";
    }

    public static String testInteger() {
        assert (Integer.bitCount(10) == 2) : "Error in bitCount";
        assert (Integer.bitCount(56456) == 7) : "Error in bitCount";
        assert (Integer.bitCount(678) == 5) : "Error in bitCount";
        assert (Integer.bitCount(235) == 6) : "Error in bitCount";
        assert (Integer.bitCount(0xFFF) == 12) : "Error in bitCount";
        Integer i = new Integer(5000);
        //assert(i.byteValue()==-120):"Error in byteValue";
        assert (i.compareTo(new Integer(5000)) == 0) : "Error in compareTo";
        assert (i.compareTo(new Integer(5001)) == -1) : "Error in compareTo";
        assert (i.compareTo(new Integer(4600)) == 1) : "Error in compareTo";
        assert (Integer.decode("5000").compareTo(i) == 0) : "Error in decode";
        assert (i.doubleValue() == 5000) : "Error in double";
        assert (i.equals(new Integer(5000))) : "Error in equals";
        assert (!i.equals(new Integer(500))) : "Error in equals";
        assert (i.floatValue() == 5000) : "Error in floatValue";
        assert (Integer.highestOneBit(400) == 256) : "Error in highestOneBit";
        assert (i.intValue() == 5000) : "Error in intValue";
        assert (i.longValue() == 5000) : "Error in longValue";
        char[] a = new char[]{34, 56};

        return "OK";
    }

    public static String testAutoBoxing() {
        
        Integer wi = new Integer(1);
        Integer wj = new Integer(2);
        Integer wk = new Integer(3);
        assert(wi instanceof Integer):"Wrong instance of for Wrapper class";
        int ni = 4, nj = 5, nk = 6;
        wi = ni + nj;      /// I = i + i
        assert(wi instanceof Integer):"Wrong instance of for Wrapper class";
        assert(wi == 9):"Wrong value for wrapper class";
        ni = wi + nj;      /// i = I + i     
        assert(ni == 14):"Wrong value for basic type";
        ni = wi + wj;   /// i = I + I
        assert(ni == 11):"Wrong value for basic type";
        wi = wj + wk;    /// I = I + I
        assert(wi instanceof Integer):"Wrong instance of for Wrapper class";
        assert(wi == 5):"Wrong value for wrapper type";        
        assert(wi instanceof Integer):"Wrong instance of for Wrapper class";
        wi = ni + wj;  /// I = i + I
        assert(wi instanceof Integer):"Wrong instance of for Wrapper class";
        ni = wi + wj;  /// i = I + I
        
        wj = (int)4;
        assert(wj instanceof Integer):"Wrong instance of for Wrapper class";
        assert(wj == 4):"Wrong value for wrapper class";
        ni = ++wj;
        assert(ni == 5):"Wrong value for basic type";
        assert(wj == 5):"Wrong value for wrapper class";
        wj = (int)4;
        ni = wj++;
        assert(wj == 5):"Wrong value for wrapper class";
        assert(ni == 4):"Wrong value for basic type";         
        
        Byte b=new Byte((byte)4);
        
        
        return "OK";
    }

    public static String externCreatorTest() {
        Integer a = new Integer(45);
        Object k = new Object();
        assert (a instanceof Number) : "Error in instance of";
        assert (a instanceof Comparable) : "Error in instance of";
        assert (!(k instanceof Number)) : "Error in instance of";
        ExCreator c = new ExCreator(10);
        assert (c.getAccess().getValue(4) == 10) : "Error in Ex Creator";
        ExCreator c1 = new ExCreator(100);
        assert (c1.getAccess().getValue('a').equals("")) : "Error in Ex Creator";
        ExCreator c2 = new ExCreator(1000);
        assert (c1.getAccess().getAnotherAccess().getValue(5) == 100) : "Error in recusrive class creator";

        return "OK";
    }

    public static String exceptionTest() {

        String s;
        try {
            try {
                int k = 7 / 0;
                Exception e = new IndexOutOfBoundsException("Indes is Out Of Bound");
                throw e;
            } catch (IndexOutOfBoundsException e) {
                throw new Exception(e);
            }
        } catch (IndexOutOfBoundsException e) {
        } catch (Exception e) {
            s = e.getMessage();
        } finally {
            s = "Exception handled Properly";
        }
        return s;
    }
    public static int staticValue = 100;
    public int value;

    public Test() {
        value = 20;
    }

    public static String staticMemberTest() {
        staticValue = 10;
        assert (staticValue == 10) : "Error in static member test";
        staticMemberTest2();
        assert (staticValue == 100) : "Error in static member test";
        staticMemerTest_instance();
        return "OK";
    }

    public static void staticMemberTest2() {
        staticValue = 100;
    }

    public static void staticMemerTest_instance() {
        Test s = new Test();
        staticValue = 10;
        s.staticMemberTest2();
        assert (Test.staticValue == 100) : "Error in static member test";

    }

    public static void innerCreatorTest() {
        InnerCreator ic = new InnerCreator(20);
        InnerCreator.Inner inner1 = ic.new Inner(34);
        InnerCreator.Inner inner2 = ic.new Inner(36);
        assert (inner1.getInnerValue() == 34) : "Error in Creating inner Class";
        assert (inner2.getInnerValue() == 36) : "Error in Creating inner Class";
        assert (inner1.getOuterValue() == 20) : "Error in Creating inner Class";
        assert (ic.getInnerInstanceWithSelf().getOuterValue() == 20) : "Error in creating innerClass";
        ic.createIC();
        assert (ic.getInnerInstance2().getOuterValue() == 800) : "Error in creating innerClass";
        assert (ic.getInnerInstance2().getInnerValue() == 34) : "Error in creating innerClass";
    }

    public static void closureTest() {
        assert (createClosure(40).getValue(4) == 40) : "Closur Error";
        assert (createClosure(100).getValue(4) == 100) : "Closur Error";

    }

    public static ExCreator.Access createClosure(final int para) {
        return new ExCreator.Access() {

            public int getValue(int a) {
                return para;
            }

            public String getValue(char k) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public Access getAnotherAccess() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    public static void enumTest() {
        EnumTest.hello();
    }

    public static String test() throws Exception {
        //UITest.Test();
        Test.testAutoBoxing();
        SuperTester.test();
        InterfaceTest.test();
        WrapperTest.test();
        ForEachTest.test();
        ByteTest.testByte();
        BooleanTest.test();
        MethodArgumentTest.testTYU();
        OverrideTest.test();
        HashMapTest.test();
        ArrayListTest.test();
        enumTest();
        closureTest();
        testString();
        testCharacter();
        testInteger();
        externCreatorTest();
        innerCreatorTest();
        exceptionTest();
        staticMemberTest();
        return "OK";
    }
}
