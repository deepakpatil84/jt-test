/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author Deepak Patil
 */
public class EnumTest2 {

    EnumTest.MyEnum2 m = EnumTest.MyEnum2.ONE;

    public EnumTest2() {
        int k = 30;
        switch (m) {
            case ONE: {
                k = 20;
                break;
            }
            case TWO1: {
                k = 30;
                break;
            }
            default: {
                k = 40;
            }
        }
    }
}
