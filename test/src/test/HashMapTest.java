/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author Deepak Patil
 */
public class HashMapTest {
     public  static boolean test() throws Exception {
         HashMap<String,String> hash=new HashMap<String, String>();
         assert(hash.isEmpty()):"size error";
         String key="hello";
         String value="jstype";
         hash.put(key,value);
         assert(hash.isEmpty()==false):"size error";
         assert(hash.size()==1):"size error";
         assert(hash.containsKey(key)):"containsKey Error";
         assert(!hash.containsKey(value)):"containsKey Error";
         assert(!hash.containsKey(null)):"containsKey Error";
         assert(hash.containsValue("jstype")):"ContaintsValue Error";
         assert(hash.put(null, "DEEPAK")==null):"Error in putting null";
         assert(hash.containsKey(null)):"containsKey Error";
         assert(hash.put(null, "jstype").equals("DEEPAK")):"Error in putting null";
         assert(hash.put(null, null)!=null):"Error in putting null";
         assert(hash.size()==2):"Error in size";
         assert(hash.remove(null)==null):"Error in removig null";
         assert(hash.size()==1):"Error in size";
         
         //entry set
         
         
         return true;
     }

}
