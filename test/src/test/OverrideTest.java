/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class OverrideTest {
    static class One{
        int x;
        public One(int v){
            x=v;
        }
        public int compute(){
            return x+1;
        }
    }
    static class Two extends One{
        int x;
        public Two(int v){
            super(v);

        }
        @Override
        public int compute(){
            return x+2;
        }
    }
    public static void test(){
        One o=new One(45);
        Two t=new Two(45);
        assert(o.compute()==46):"Error in override";
        assert(t.compute()==2):"Error in override";
        o=t;
        assert(o.compute()==2):"Error in override";
    }

}
