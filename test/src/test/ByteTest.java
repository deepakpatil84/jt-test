/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author Deepak Patil
 */
public class ByteTest {

    public static void testByte() {
        Byte b = new Byte((byte) 34);
        assert (b.byteValue() == 34) : "Error in byte constroctor";
        b = new Byte("34");
        assert (b.byteValue() == 34) : "Error in byte constroctor";
        Byte bl = new Byte((byte) 10);
        Byte bh = new Byte((byte) 100);
        
        assert (b.compareTo(b) == 0) : "Error in compareTo";
        assert (b.compareTo(bl) == 1) : "Error in compareTo";
        assert (b.compareTo(bh) == -1) : "Error in compareTo";
        byte nb=Byte.decode("34");
        assert (Byte.decode("34") == 34) : "Error in decode";
        try {
            Byte.decode("---");
            assert (false) : "Error in decode for invalide string";
        } catch (NumberFormatException e) {
        } catch (Exception e) {
            assert (false) : "Exception error in decode";
        }
        assert(b.doubleValue()==34):"Error in doubleValue";
        assert(b.equals(bl)==false):"Error in equals";
        assert(b.equals(b)==true):"Error in equals";
        assert(b.floatValue()==34):"Error in floatValue";
        assert(b.hashCode()==34):"Error in hashCode";
        assert(b.intValue()==34):"Error in intValue";
        assert(b.longValue()==34):"Error in intValue";
        assert(Byte.parseByte("34")==34):"Error in parseByte";
        assert(Byte.parseByte("22",16)==34):"Error in parseByte";
        assert(b.shortValue()==34):"Error in shortValue";
        assert(b.toString().equals("34")):"Error in toString";
        assert(Byte.toString((byte)34).equals("34")):"Error in static toString";
        assert(Byte.valueOf((byte)34).byteValue()==34):"Error in valueOf(byte)";
        assert(Byte.valueOf("34").byteValue()==34):"Error in valueOf(byte)";
        assert(Byte.valueOf("22",16).byteValue()==34):"Error in valueOf(byte)";
    }
    public void testWrapper(){
        int k=4;
        
    }
}
