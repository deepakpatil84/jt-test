/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import java.util.ArrayList;

/**
 *
 * @author Deepak Patil
 */
public class ForEachTest {
    public static void test(){
       int[] a=new int[]{1,2,3,4};
       int total=0;
       for(int k:a){
           total+=k;
       }
       assert(total==10):"Error in for each for array";
       ArrayList<Integer> list=new ArrayList<Integer>();
       list.add(new Integer(3));
       list.add(new Integer(4));
       total=0;
       for(Integer wi:list){
            total+=wi;
       }
       assert(total==7):"Error in for each for iterable";
       
       
    }

}
