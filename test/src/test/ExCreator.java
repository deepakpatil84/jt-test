/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class ExCreator {

    private int value;
    public static interface Access {
        int getValue(int a);
        String getValue(char k);
        Access getAnotherAccess();
    }
    public ExCreator(int v){
        value=v;
    }
    public Access getAccess(){
        return new Access() {
            public String getValue(char k){
                return "";
            }
            public int getValue(int a) {
                this.process();
                return value;

            }
            public Access getAnotherAccess(){
                return new Access() {
                    public int getValue(int a) {
                        return test.ExCreator.this.value;
                    }
                    public String getValue(char k) {
                        return "welcome";
                    }

                    public Access getAnotherAccess() {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                };
            }
            public void process(){
                
            }
           
        };
    }

}
