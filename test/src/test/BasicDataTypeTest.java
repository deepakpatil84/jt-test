/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author Deepak Patil
 */
public class BasicDataTypeTest {
    //byte
    //char
    //short
    //int
    //long
    //float
    //double
    //boolean
    //void

    public static void test() {
        testBitwiseNot();
    }

    public static void testBitwiseNot() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
        b = (byte) ~b;
        c = (char) ~c;
        s = (short) ~s;
        l = ~l;
        //TODO:these test cases are wrong
        assert (~b == 4) : "Err(~)";
        assert (~1 == -2) : "Errot in binary not(~)";
        assert (~5 == -6) : "Errot in binary not(~)";
        //TODO:add test cases for negative number
    }

    public static void testLogicalNot() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
        assert (!bool == false) : "Error in Logical Not(!)";
    }

    public static void testIncrementDecrement() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;

    }

    public static void testOperatorNegateAndPositive() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
        b = (byte) +b;
        c = (char) +c;
        s = (short) +s;
        i = +i;
        l = +l;
        f = +f;
        d = +d;
    }

    public static void testOperatorMultiplicative() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;

    }

    public static void testOperatorAdditive() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
    }

    public static void testOperatorShift() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
    }

    public static void testOperatorRelational() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
    }

    public static void testOperatorEquality() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
    }

    public static void testOperatorBitwise() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
    }

    public static void testOperatorAssignment() {
        byte b = (byte) 1;
        char c = 'a';
        short s = (short) 5;
        int i = 4;
        long l = 4;
        float f = (float) 4.5;
        double d = 4.5;
        boolean bool = true;
        b /= l;
        c /= f;
        c %= f;
        s /= s;
        i /= i;
        d = d % d;
        l *= b;
        b &= b;
        c &= c;
        s &= s;
        i &= l;
        b &= i;
        Integer wi = new Integer(45);
        Double wd = new Double(45);
        Number n = wi;
        BasicDataTypeTest bdt1 = new BasicDataTypeTest();
        Object o;
    }
}
