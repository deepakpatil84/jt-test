/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import java.util.ArrayList;

/**
 *
 * @author Deepak Patil
 */
public class ArrayListTest {

    public  static boolean test() throws Exception {
        ArrayList<Integer> list=new ArrayList<Integer>();
        assert(list.size()==0):"Error in array size";
        assert(list.isEmpty()):"Error in array size";
        assert(list.add(new Integer(34))==true):"Error in adding value";
        assert(list.isEmpty()==false):"Error in array size";
        assert(list.size()==1):"Error in array size";
        assert(list.get(0).intValue()==34):"Error in getting value";
        assert(list.add(new Integer(50))==true):"Error in adding value";
        assert(list.size()==2):"Error in array size";
        assert(list.get(0).intValue()==34):"Error in getting value";
        assert(list.get(1).intValue()==50):"Error in getting value";
        list.add(0,new Integer(100));
        assert(list.get(0).intValue()==100):"Error in getting value";
        assert(list.get(1).intValue()==34):"Error in getting value";
        assert(list.get(2).intValue()==50):"Error in getting value";
        assert(list.size()==3):"Error in array size";
        ArrayList<Integer> list2=new ArrayList<Integer>();
        //this will return false as list is not modified
        assert(list.addAll(list2)==false):"Error in add all";
        assert(list.size()==3):"Error in array size";
        list2.add(new Integer(300));
        Integer i=new Integer(500);
        list2.add(i);
        assert(list.addAll(list2)):"Error in add all";
        assert(list.size()==5):"Error in array size";
        assert(list.get(3).intValue()==300):"Error in getting value";
        assert(list.get(4).intValue()==500):"Error in getting value";

        ArrayList<Integer> list3;
        list3=(ArrayList<Integer>)list.clone();
        assert(list.size()==list3.size()):"Clone error";
        list.clear();
        assert(list.isEmpty()):"Clear error";
        assert(list3.contains(i)):"Contains Error";
        assert(list3.indexOf(i)==4):"Index Error";
        assert(list3.indexOf(null)==-1):"IndexOf Error";
        Integer i2=new Integer(57);
        assert(list3.indexOf(i2)==-1):"IndexOf Error";
        list3.add(i);
        assert(list3.lastIndexOf(i)==5):"lastIndexOf Error";
        list3.add(i2);
        assert(list3.remove(6).intValue()==57):"remove(index) error";
        assert(list3.remove(i)):"remove(object) error";
        assert(list3.size()==5):"size after remove error";
        Object[] os=list3.toArray();
        assert(os.length==5):"Error in toArray";
        assert(((Integer)(os[0])).intValue()==100):"Error in toArray";

        Integer[] is=new Integer[list3.size()];
        list3.<Integer>toArray(is);
        //TODO:toArray,trimToSize
        return true;
    }

}
