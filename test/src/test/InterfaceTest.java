/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;
public class InterfaceTest {
    public static interface TestInterface{
        public int get();
    }
    public static class Impl1 implements TestInterface{
        public int get(){
            return 1;
        }
    }
    public static class Impl2 implements TestInterface{
        public int get(){
            return 2;
        }
    }
    public static class Impl3 extends Impl2 implements TestInterface{
        @Override
        public int get(){
            return 3;
        }
    }
    public static void test(){
        Impl1 o1=new Impl1();
        Impl2 o2=new Impl2();
        TestInterface t;
        t=o1;
        assert(t.get()==1):"Error in abstract call";
        t=o2;
        assert(t.get()==2):"Error in abstract call";
        t=new Impl3();
        assert(t.get()==3):"Error in abstract call";
    }
}
