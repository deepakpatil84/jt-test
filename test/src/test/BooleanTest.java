package test;

public class BooleanTest {
    public static void test(){
        Boolean wt=new Boolean(true);
        Boolean wf=new Boolean(false);

        assert(wt.booleanValue()==true):"Error in boolean";
        assert(wf.booleanValue()==false):"Error in boolean";
        assert(wt.equals(wt)):"Error in equals";
        assert(!wt.equals(wf)):"Error in equals";
        assert(wt.hashCode()==1231):"Error in hashCode";
        assert(wf.hashCode()==1237):"Error in hashCode";
        assert(Boolean.parseBoolean("true")):"Error in parseBoolean";
        assert(!Boolean.parseBoolean("false")):"Error in parseBoolean";
        assert(Boolean.parseBoolean("TruE")):"Error in parseBoolean";
        assert(!Boolean.parseBoolean("jstype")):"Error in parseBoolean";
        assert(wt.toString().equals("true")):"Error in toString";
        assert(wf.toString().equals("false")):"Error in toString";
        assert(Boolean.toString(true).equals("true")):"Error in toString";
        assert(Boolean.toString(false).equals("false")):"Error in toString";
        assert(Boolean.valueOf(true).equals(wt)):"Error in valueOf";
        assert(Boolean.valueOf(false).equals(wf)):"Error in valueOf";
        assert(Boolean.valueOf("true").equals(wt)):"Error in valueOf";
        assert(Boolean.valueOf("truE").equals(wt)):"Error in valueOf";
        assert(Boolean.valueOf("false").equals(wf)):"Error in valueOf";
        assert(Boolean.valueOf("welcome").equals(wf)):"Error in valueOf";
        assert(wt.compareTo(wt)==0):"Error in compareTo";
        assert(wf.compareTo(wf)==0):"Error in compareTo";
        assert(wt.compareTo(wf)==1):"Error in compareTo";
        assert(wf.compareTo(wt)==-1):"Error in compareTo";



        
    }
}
