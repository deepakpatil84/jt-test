package test;

public class MethodArgumentTest {

    static interface Inf {

        public void hello();
    }

    static class One<E extends Inf> implements Inf {

        public <T> T get(T k) {
            return k;
        }

        public void hello() {
        }
    }

    static class Two<E extends Inf> extends One {
    }

    public static void testTYU() {
        One<Inf> o = new One<Inf>();
        Two t = new Two();
        o.<Two>get(t).hello();
        
    }
}
