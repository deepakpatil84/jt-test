/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

/**
 *
 * @author Deepak Patil
 */
public class WrapperTest {
    public static int testInteger(Integer i){
        return i+60;
    }
    public static int testInteger(int i){
        return i+40;
    }
    public static long testInteger(long i){
        return i+50;
    }
    public static Integer testInteger2(Integer i){
        return i+60;
    }
    public static Integer testInteger2(int i){
        return i+40;
    }
    public static int testAutoBox(Integer i){
        return i;
    }
    public static Integer testAutoUnBox(int i){
        return i;
    }
    public static void test(){
        assert(testInteger(new Integer(40))==100):"Error in wrapper calss processing";
        assert(testInteger(60)==100):"Error in wrapper calss processing";
        assert(testInteger((long)60)==110):"Error in wrapper calss processing";
        assert(testInteger2(new Integer(40))==100):"Error in wrapper calss processing";
        assert(testInteger2(60)==100):"Error in wrapper calss processing";
        assert(testAutoBox(23)==23):"Error in autoboxing";
        assert(testAutoUnBox( new Integer(45))==45):"Error in unboxing";
    }

}
