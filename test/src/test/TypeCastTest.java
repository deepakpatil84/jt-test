/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;



/**
 *
 * @author SOFT
 */
public class TypeCastTest {
    static class MyClass<M,V>{
        public M getTypeOne(Object o){
            return (M)o;
        }
    }
    static class InnerClass{
        
    }
    public static void test() {
        String t = "";
        String m;
        if (true) {
            t = null;
        }
        String[] array=new String[10];
        int l=array.length;
       

        m = (String) null;
        MyClass cls=new MyClass<InnerClass,String>();
        cls.getTypeOne(new InnerClass());
        cls.getTypeOne(m);
        cls.getTypeOne(0);
        //cls.getT
        //System.out.println(cls.getTypeOne(1));
        
    }
}
