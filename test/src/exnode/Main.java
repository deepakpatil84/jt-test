package exnode;

import com.jstype.core.JSObject;

import test.Test;
import node.core.Console;
import node.core.Module;
import node.core.Process;

public class Main extends Module{
	static {
		run();
	}

	public static void run() {
		//Console console = Console.getConsole();
		//Process process = Process.getPrcess();
		Console.log(Process.getExecPath());
		Console.log("Hello");
		try {
			Console.log(Test.test());
		} catch (Exception e) {
			Console.log(e);
		}
		Console.log(Process.getPid());
		JSObject jso=Process.getVersions();
		Console.log(jso);
		Process.exit();
	}
}
